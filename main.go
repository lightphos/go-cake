// # Cake Manager System Overview
//
// ## Tech Stack & Tools
// - Golang
// - Sqlite
// - bld (make like build tool)
// - goasciidoc
//
// ## Component Architecture
// [plantuml, diagram-classes, png]
// ....
//
// [main]
// [rest]
// main -> rest
// rest -> Api
//
//	package "db" {
//	    [provision] .. Accesser
//	    [download] .. Accesser
//	    [db-operation] .. Accesser
//	}
//	package "service" {
//		[services] ... Svcer
//		[services] --> Accesser
//	}
//
//
//	package "api" {
//		 [handler] ... Api
//		 [handler] --> Svcer
//	}
//
// ....
// ## Sequence
// [plantuml, diagram-sequence, png]
// ....
// participant Client
// participant API
// participant Service
// participant Database
//
// Client -> API: Request cakes
// API -> Service: Process request
// Service -> Database: Get cakes
// Database --> Service: Response
// Service --> API: Response
// API --> Client: Response cake list
// ....
// ## Main
// Swagger API:
// @title Cake API
// @version 1.0
// @description API to manage cakes
// @host localhost:8282
// @BasePath /
//
// This section is responsible for initializing the API service with necessary dependencies.
// It includes creating a new database connection, connecting to the database, creating tables if they don't exist,
// and finally creating a new service and API instance with the established database connection.
// The API is then instructed to download cakes, preparing the system for handling cake-related requests.
package main

import (
	"go-cake/api"
	"go-cake/db"
	svc "go-cake/service"
)

var rest *api.Api

func init() {
	dba := db.New()
	svc := svc.New(dba)
	rest = api.New(svc)
	rest.DownloadCakes()
}

func main() {
	println("Cakes API")

	rest.Handlers()
}
