// Package api provides rest endpoints for the CakeManager application.
package api

import (
	"encoding/json"
	"fmt"
	d "go-cake/domain"
	s "go-cake/service"
	"net/http"
	"strconv"

	"github.com/swaggo/http-swagger/v2"

	_ "go-cake/docs"
)

// Api represents the service for the api endpoints to use.
type Api struct {
	svc s.Svcer
}

// New instantiates Api.
func New(svcer s.Svcer) *Api {
	return &Api{svc: svcer}
}

func basicAuth(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		fmt.Printf("Auth %s, %s, %t\n", username, password, ok)

		if !ok || !validateCredentials(username, password) {
			w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
			http.Error(w, "Unauthorized.", http.StatusUnauthorized)
			return
		}
		handler(w, r)
	}
}

func validateCredentials(username, password string) bool {
	return username == "admin" && password == "admin"
}

// @Summary Get all cakes
// @Description Retrieve all cakes
// @Produce json
// @Success 200 {object} []d.Cake
// @Router /cakes [get]
func (api Api) handleListCakes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	cakes, err := api.svc.ReadAll()
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(cakes)
}

// @Summary Get a specific cake by ID
// @Description Retrieve a cake by its ID
// @Produce json
// @Param id path int true "Cake ID"
// @Success 200 {object} d.Cake
// @Router /cakes/{id} [get]
func (api Api) handleGetCake(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := r.PathValue("id")
	intId, _ := strconv.Atoi(id)
	cake, err := api.svc.Read(intId)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(cake)
}

// @Summary Add a new cake
// @Description Add a new cake to the database
// @Produce json
// @Success 200 "ok"
// @Router /cakes [post]
func (api Api) handleAddCake(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var cake = d.Cake{}

	err := json.NewDecoder(r.Body).Decode(&cake)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = api.svc.Write(cake)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("ok")
}

func (api Api) handlePopCake(w http.ResponseWriter, r *http.Request) {
	api.DownloadCakes()

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode("ok")
}

// @Summary Update an existing cake
// @Description Update an existing cake in the database
// @Produce json
// @Success 200 "ok"
// @Router /cakes/{id} [put]
func (api Api) handleUpdateCake(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var cake = d.Cake{}
	err := json.NewDecoder(r.Body).Decode(&cake)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	id := r.PathValue("id")
	cake.ID = id

	err = api.svc.Update(cake)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok\n"))
}

// @Summary Remove a cake by ID
// @Description Remove a cake from the database by its ID
// @Produce json
// @Param id path int true "Cake ID"
// @Success 200 "ok"
// @Router /cakes/{id} [delete]
func (api Api) handleRemoveCake(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	id := r.PathValue("id")

	intId, _ := strconv.Atoi(id)
	err := api.svc.Remove(intId)
	if err != nil {
		fmt.Printf("%v\n", err) // todo error management
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok\n"))
}

// Handlers sets up HTTP handlers for different API endpoints.
func (api Api) Handlers() {
	// Define OpenAPI endpoint for Swagger documentation
	http.Handle("/swagger/", httpSwagger.Handler(httpSwagger.URL("http://localhost:8282/swagger/doc.json")))

	http.Handle("GET /cakes", http.HandlerFunc(api.handleListCakes))
	http.Handle("GET /cakes/{id}", http.HandlerFunc(api.handleGetCake))
	http.Handle("POST /cakes/populate", http.HandlerFunc(api.handlePopCake))
	http.Handle("POST /cakes", http.HandlerFunc(api.handleAddCake))
	http.Handle("PUT /cakes/{id}", http.HandlerFunc(api.handleUpdateCake))
	http.Handle("DELETE /cakes/{id}", basicAuth(api.handleRemoveCake))

	fmt.Println("Serving on 8282")
	http.ListenAndServe(":8282", nil)

}

// DownloadCakes fetches cakes data from an external URL and writes it using the service.
func (api *Api) DownloadCakes() {

	api.svc.Provision()

	cakeURL := "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/" +
		"8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"

	resp, err := http.Get(cakeURL)
	if err != nil {
		fmt.Println("Error making GET request:", err)
		return
	}
	defer resp.Body.Close()

	var cakes []d.Cake
	err = json.NewDecoder(resp.Body).Decode(&cakes)
	if err != nil {
		fmt.Println("Error unmarshalling JSON:", err)
		return
	}

	api.svc.WriteAll(cakes)

}
