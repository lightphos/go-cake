package api

import (
	"errors"
	d "go-cake/domain"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockSvc struct {
	mock.Mock
}

func (m *MockSvc) ReadAll() ([]d.Cake, error) {
	args := m.Called()
	return args.Get(0).([]d.Cake), args.Error(1)
}

func (m *MockSvc) Read(id int) (d.Cake, error) {
	// Implement mock behavior for Read method
	return d.Cake{}, errors.New("Read method not implemented in mock")
}

func (m *MockSvc) WriteAll(cakes []d.Cake) {
	// Implement mock behavior for WriteAll method
}

// Write mocks the Write method.
func (m *MockSvc) Write(cake d.Cake) error {
	// Implement mock behavior for Write method
	return errors.New("Write method not implemented in mock")
}

// Update mocks the Update method.
func (m *MockSvc) Update(cake d.Cake) error {
	// Implement mock behavior for Update method
	return errors.New("Update method not implemented in mock")
}

// Remove mocks the Remove method.
func (m *MockSvc) Remove(id int) error {
	// Implement mock behavior for Remove method
	return errors.New("Remove method not implemented in mock")
}

func (m *MockSvc) Provision() {
}

func TestHandleListCakes(t *testing.T) {
	mockSvc := new(MockSvc)
	api := New(mockSvc)

	expectedCakes := []d.Cake{{ID: "1", Title: "Chocolate Cake", Desc: "nice"}}
	mockSvc.On("ReadAll").Return(expectedCakes, nil)

	req, err := http.NewRequest("GET", "/cakes", nil)
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(api.handleListCakes)

	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)
	mockSvc.AssertExpectations(t) // Verify that ReadAll was called
}
