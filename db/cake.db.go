// Package db provides database operations for the CakeManager application.
package db

import (
	"fmt"
	d "go-cake/domain"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

// Connecter defines the methods for connecting to and closing a database.
type Accesser interface {
	Connect()
	Close()
	Insert(cake d.Cake) error
	Update(cake d.Cake) error
	Get() ([]d.Cake, error)
	GetForId(id int) (d.Cake, error)
	Delete(id int) error
}

type Provisioner interface {
	MustCreateTables()
	Populate(cakes []d.Cake)
}

// Access represents a database reference with methods for interacting with the database.
type Access struct {
	Provisioner
	db *sqlx.DB
}

// New creates a new instance of Access.
func New() Accesser {
	a := &Access{}
	a.Connect()
	return a
}

// Setdb sets the db
func Setdb(s *Access, db *sqlx.DB) {
	s.db = db
}

// Insert adds a new cake to the database.
func (s *Access) Insert(cake d.Cake) error {
	query := "INSERT INTO cakes (title, desc, image) VALUES (:title, :desc, :image)"
	_, err := s.db.NamedExec(query, cake)
	return err
}

// Update updates an existing cake in the database.
func (s *Access) Update(cake d.Cake) error {
	query := "UPDATE cakes SET title = :title, desc = :desc, image = :image WHERE id = :id"
	_, err := s.db.NamedExec(query, cake)
	return err
}

// Get retrieves all cakes from the database.
func (s *Access) Get() ([]d.Cake, error) {
	cakes := []d.Cake{}
	err := s.db.Select(&cakes, "SELECT id, title, desc, image FROM cakes")
	if err != nil {
		return nil, err
	}
	return cakes, nil
}

// GetForId retrieves a specific cake by its ID from the database.
func (s *Access) GetForId(id int) (d.Cake, error) {
	cakes := []d.Cake{}
	err := s.db.Select(&cakes, "SELECT id, title, desc, image FROM cakes where id = ?", id)
	if err != nil || len(cakes) == 0 {
		return d.Cake{}, fmt.Errorf("failed to read %d", id)
	}
	return cakes[0], nil
}

// Delete removes a cake from the database by its ID.
func (s *Access) Delete(id int) error {
	_, err := s.db.Exec("DELETE FROM cakes where id = ?", id)
	return err
}

// Connect establishes a connection to the database.
func (s *Access) Connect() {
	db, err := sqlx.Open("sqlite3", "cake.db")
	if err != nil {
		log.Fatal(err)
	}
	s.db = db
	fmt.Printf("Connected %v\n", db)
}

// Close closes the connection to the database.
func (s *Access) Close() {
	s.db.Close()
	fmt.Println("Closed")
}

// MustCreateTables creates the necessary tables in the database if they do not exist.
func (s *Access) MustCreateTables() {
	_, err := s.db.Exec("DROP TABLE IF EXISTS cakes")
	if err != nil {
		panic(err)
	}
	_, err = s.db.Exec("CREATE TABLE IF NOT EXISTS cakes (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
		"title VARCHAR(255) UNIQUE, desc varchar(255), image varchar(255))")
	if err != nil {
		panic(err)
	}
	fmt.Println("Tables created successfully.")
}

// Populate inserts multiple cakes into the database.
func (s *Access) Populate(cakes []d.Cake) {
	for _, cake := range cakes {
		err := s.Insert(cake)
		if err != nil {
			fmt.Println(err)
		}
	}
	fmt.Println("Data written.")
}
