# Cake Manager

Written in Go v1.22.

Get bld tool from:
https://gitlab.com/lightphos/bld/-/releases

## How to run

`bld run`
> go run main.go

`bld list`
> curl -X GET localhost:8282/cakes

```
bld cycle

bld pop
bld list
bld add
bld get
bld upd
bld get
bld del
```

Delete has basic auth middleware behind it: 
    username: admin, password: admin.


NOTE: Uses sqlite db (cakes.db), which is populated on initialization.

See also: build.bld file.

## Docs

bld adoc (ascii docs)

Includes component and sequence diagram and all the go docs. 

- See adoc directory

  > In VS Code install the ASCII Doc extension to view the docs in the IDE
  > 


bld gdoc
- http://localhost:6060/pkg/go-cake/

## Test

bld test


### Coverage

bld report

### Swagger

bld swag
- http://localhost:8282/swagger/index.html
- http://localhost:8282/swagger/doc.json

