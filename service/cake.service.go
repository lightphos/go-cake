// Package service layer between the client and resources, with any business logic as needed.
package service

import (
	"go-cake/db"
	d "go-cake/domain"
)

type Svcer interface {
	ReadAll() ([]d.Cake, error)
	Read(id int) (d.Cake, error)
	WriteAll(cakes []d.Cake)
	Write(cake d.Cake) error
	Update(cake d.Cake) error
	Remove(id int) error
	Provision()
}

// Svc represents the service layer for cake operations.
type Svc struct {
	db db.Accesser
}

// New creates a new instance of Svc with the provided database connection.
func New(db db.Accesser) Svcer {
	return &Svc{db: db}
}

// WriteAll writes multiple cakes to the database.
func (svc *Svc) WriteAll(cakes []d.Cake) {
	svc.db.(db.Provisioner).Populate(cakes)
}

// Write adds a new cake to the database.
func (svc *Svc) Write(cake d.Cake) error {
	return svc.db.Insert(cake)
}

// Update updates an existing cake in the database.
func (svc *Svc) Update(cake d.Cake) error {
	return svc.db.Update(cake)
}

// GetAll retrieves all cakes from the database.
func (svc *Svc) ReadAll() ([]d.Cake, error) {
	// some biz logic and biz error handling
	return svc.db.Get()
}

// Get retrieves a specific cake by ID from the database.
func (svc *Svc) Read(id int) (d.Cake, error) {
	return svc.db.GetForId(id)
}

// Remove deletes a cake by ID from the database.
func (svc *Svc) Remove(id int) error {
	return svc.db.Delete(id) // some biz logic and biz error handling
}

func (svc *Svc) Provision() {
	svc.db.(db.Provisioner).MustCreateTables()
}
