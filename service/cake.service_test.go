package service

import (
	"database/sql"
	"fmt"
	"go-cake/db"
	d "go-cake/domain"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
)

type MockConnection struct {
	db.Access
	mock   sqlmock.Sqlmock
	mockDb *sql.DB
}

var mc MockConnection = MockConnection{}

func (mc MockConnection) Connect() {
	fmt.Println("Mock db connection")
}

func (mc MockConnection) Close() {
	fmt.Println("Mock close connection")
	mc.mockDb.Close()
}

func setup() {
	mockDB, mock, _ := sqlmock.New()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")
	db.Setdb(&mc.Access, sqlxDB)
	mc.mock = mock
	mc.mockDb = mockDB
}

func TestWriteAll(t *testing.T) {
	setup()
	svc := New(&mc.Access)
	mc.mock.ExpectExec("INSERT INTO cakes \\(title, desc, image\\) VALUES \\(\\?, \\?, \\?\\)").WillReturnResult(sqlmock.NewResult(1, 1))

	cakes := []d.Cake{{ID: "1", Title: "Chocolate Cake"}, {ID: "2", Title: "Vanilla Cake"}}
	svc.WriteAll(cakes)
	// Add assertions to check if the cakes were successfully written to the database
}

func TestWrite(t *testing.T) {
	setup()
	svc := New(&mc.Access)
	mc.mock.ExpectExec("INSERT INTO cakes \\(title, desc, image\\) VALUES \\(\\?, \\?, \\?\\)").WillReturnResult(sqlmock.NewResult(1, 1))

	cake := d.Cake{ID: "1", Title: "Strawberry Cake"}
	err := svc.Write(cake)
	if err != nil {
		t.Errorf("Write failed with error: %v", err)
	}
	// Add assertions to check if the cake was successfully written to the database
}

// Add similar test functions for Update, GetAll, Get, and Remove functions
